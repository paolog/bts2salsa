## bts2salsa

- python3 script to keep the salsa.debian.org issue tracker aligned with the debian BTS
- license: GPLv3+
- home: https://salsa.debian.org/paolog-guest/bts2salsa
- author: Paolo Greppi <paolo.greppi@libpf.com>

I am planning to use the salsa issue tracker and kanban board to ease my job as a maintainer (project management, tracking "internal" maintainership priorities).
This is probably only useful for packages with more than a handful of bugs open.

The debian BTS is the authoritative database, and it already has triggers for closing bugs, via debian/changelog (Closes: #XXXXX).

Here is how to keep the salsa issue tracker in line with the BTS:

- Run this script periodically.
- Reprioritize, sort, move around in the kanboard
- Do not bother to close issue with fixes: #XX in the git commit message: close BTS bugs as usual
- Run this script again and the issue bound to a closed BTS bug should go away.

To use this, you first have to enable the issue tracker in your salsa project. 
To turn on the issue tracker in a salsa project, go to project / settings / general:
https://salsa.debian.org/owner/project/edit
then expand Permissions, turn on issues for "Everyone with access", then Save changes

Requirements: [python-gitlab](https://github.com/python-gitlab/python-gitlab), install on debian with: `sudo apt install python3-gitlab`

Status: alpha release, incomplete, buggy.

### How to use:

1. adapt the script to your needs

2. get token with via [gitlab UI](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token), save it to token file like this: 
```
echo -n aA-a-123456789012345 > token
```

**do not distribute the token file !**

3. extract basic BTS data from UDD in a file named bugs.json
```
PGPASSWORD="udd-mirror" psql --host=udd-mirror.debian.net --user=udd-mirror udd
SELECT COUNT(*) FROM bugs WHERE package like '%doxygen%'; # 52
SELECT COUNT(*) FROM archived_bugs WHERE package like '%doxygen%'; # 218
SELECT COUNT(*) FROM all_bugs WHERE package like '%doxygen%'; # 270
SELECT id,title,severity FROM bugs WHERE package like '%doxygen%' ORDER BY id;
\out bugs.json
WITH c as (SELECT id,title,severity FROM bugs WHERE package like '%doxygen%' ORDER BY id) SELECT JSON_AGG(ROW_TO_JSON(c)) from c;
```
4. manually clean up bugs.json, then prettify it and save it in a file named bugs1.json:

  python3 -m json.tool bugs.json > bugs1.json

### TODOs:

- the script currently is hardwired for doxygen package

- automate extracting the BTS bug list
